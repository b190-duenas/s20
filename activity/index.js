let number=Number(prompt("Enter a Number:"));
console.log("The number you provided is " + number);
for(let x = number; x>=0; x--){
	if(x<=50){
		console.log("The current value is at " + x + ". Terminating the loop.")
		break;
	}
	if(x%10 == 0){
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	}
	if(x%5 == 0){
		console.log(x);
	}
}

let text="supercalifragilisticexpialidocious";
let tempText=[];

console.log(text);

for(let y=0; y<text.length; y++){
	if (text[y] == "a" || text[y] == "e" ||	text[y] == "i" || text[y] == "o" ||	text[y] == "u"){
		continue;
	} else{
		tempText[y]=text[y];
	}
}
tempText = tempText.join("");
console.log(tempText);